import re

from datetime import datetime
from pydantic import BaseModel, validator


class GPSMessage(BaseModel):
    imei: str
    lat: float
    lon: float
    speed: int
    course: int
    state_at: datetime

    @validator('imei')
    def only_numbers(cls, value):
        if not re.match('^\d+$', value):
            raise ValueError('Imei must contain only numbers')
        return value
