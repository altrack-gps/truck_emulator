let routeLine = null;
let routePoints = [];
let MAP = null;


class Point {
    constructor(lat, lng) {
        this.lat = lat;
        this.lng = lng;
        this.marker = null;
    }

    draw() {
        this.marker = L.marker(this.coords(), {draggable: true}).addTo(MAP);
    }

    coords() {
        return [this.lat, this.lng];
    }

    remove() {
        this.marker.remove();
    }
}


window.onload = function () {
    initMap();
};


function initMap() {
    MAP = L.map('map').setView([48.50895420281152, 34.61002349853516], 13);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiYnViYXRlYW0iLCJhIjoiY2pyYzNsNTJzMTY4ODN6bDhlMzRpOHRlayJ9.rt-HxxiLNbEbAy1Vzg1foQ'
    }).addTo(MAP);

    MAP.addEventListener('click', (e) => createPoint(e.latlng));
}

function createPoint({lat, lng}) {
    let point = new Point(lat, lng);
    routePoints.push(point);
    point.draw();
}


function buildRoute() {
    getRoute();
}


function getRoute() {
    let coordString = routePoints.reduce(
        (str, point) => str + `${point.coords()[1]},${point.coords()[0]};`, ''
    );
    coordString = coordString.slice(0, -1);
    const url = `http://localhost:5000/route/v1/driving/${coordString}?geometries=geojson`;

    let request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let routeInfo = JSON.parse(request.response);
            drawRoute(routeInfo['routes'][0]['geometry']['coordinates']);
        }
    };
    request.open('GET', url, true);
    request.send();
}


function drawRoute(coordinates) {
    routeLine = L.polyline(coordinates.map(
        coord => [coord[1], coord[0]]
    ), {color: 'red'}).addTo(MAP);
}


function reset() {
    for (let point of routePoints)
        point.remove();

    routePoints = [];
    routeLine.remove();
}