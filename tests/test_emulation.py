import asyncio
import functools

from asynctest import TestCase, patch, CoroutineMock

from utils import emulation, gps_message
from utils.emulation import EmulationManager, TruckEmulation
from tests.test_app import empty_route_response


class TestEmulation(TestCase):
    @patch('utils.emulation.gps_message.generate')
    @patch('asyncio.open_connection')
    async def test_emulation_task(self, conn_mock, generate_mock):
        imei = '1111'
        coordinates = [[0, 1]]

        generate_mock.return_value = gps_message.generate(imei, coordinates[0])

        result = asyncio.Future()
        result.set_result('test')

        conn_mock.return_value = result

        close_mock = CoroutineMock(return_value=result)
        conn_mock.wait_closed = close_mock

        write_mock = CoroutineMock(return_value=result)
        conn_mock.write = write_mock

        conn_mock.return_value = (conn_mock, conn_mock)
        with patch('asyncio.sleep'):
            await emulation._run(imei, coordinates)

        conn_mock.wait_closed.assert_awaited()
        write_mock.assert_called_with(
            gps_message.encode(
                generate_mock.return_value
            )
        )


class TestEmulationManager(TestCase):
    @patch('utils.emulation.build_route')
    async def test_task_adding(self, route_mock):
        imei = '1111'
        points = [0]
        manager = EmulationManager()

        route_mock.return_value = empty_route_response

        with patch('utils.emulation._run'):
            task = await TruckEmulation.create(imei, points, manager.remove)
            manager.add(imei, task)
            self.assertIn(task, manager.tasks.values())
            manager.cancel(imei)

        await asyncio.sleep(0.5)

    @patch('utils.emulation.build_route')
    async def test_task_cancellation(self, route_mock):
        imei = '1111'
        points = [0]
        manager = EmulationManager()

        route_mock.return_value = empty_route_response
        with patch('utils.emulation._run'):
            task = await TruckEmulation.create(imei, points, manager.remove)
            manager.add(imei, task)
            manager.cancel(imei)

            await asyncio.sleep(0.5)
            self.assertTrue(task.cancelled())
            self.assertNotIn(task, manager.tasks.values())

    @patch('utils.emulation.build_route')
    async def test_task_finishing(self, route_mock):
        imei = '1111'
        manager = EmulationManager()
        route_mock.return_value = empty_route_response

        with patch('utils.emulation._run'):
            task = asyncio.Future()
            task.add_done_callback(
                functools.partial(manager.remove, imei)
            )
            manager.add(imei, task)
            task.set_result('finished')

            await asyncio.sleep(0.5)
            self.assertTrue(task.done())
            self.assertNotIn(task, manager.tasks.values())
