import asyncio

from asynctest import patch, CoroutineMock
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp.web_exceptions import HTTPOk, HTTPCreated

from app.start import create_app
from utils.emulation import TruckEmulation


class TestApp(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @patch('utils.emulation.TruckEmulation.create')
    @unittest_run_loop
    async def test_start_emulation_view(self, create_mock):
        imei = '1111'
        coordinates = [[0, 1]]
        emulation_task = asyncio.Future()
        emulation_task.set_result(imei)
        create_mock.return_value = CoroutineMock(return_value=asyncio.Future())

        response = await self.client.put(
            self.app.router['start_emulation'].url_for(imei=imei),
            json={'points': coordinates}
        )
        self.assertEqual(
            response.status,
            HTTPCreated.status_code
        )
        manager = self.app['emulation_manager']

        self.assertEqual(
            manager.get(imei),
            create_mock.return_value
        )
        self.assertEqual(len(manager.tasks), 1)

    @patch('utils.emulation._run')
    @patch('utils.emulation.build_route')
    @unittest_run_loop
    async def test_stop_emulation_view(self, route_mock, run_mock):
        imei = '2222'
        route_mock.return_value = empty_route_response

        manager = self.app['emulation_manager']
        task = await TruckEmulation.create(
            imei=imei,
            points=[],
            on_remove=manager.remove
        )
        manager.add(imei, task)
        await self.client.put(
            self.app.router['stop_emulation'].url_for(imei=imei)
        )
        with self.assertRaises(KeyError):
            manager.get(imei)


empty_route_response = []
