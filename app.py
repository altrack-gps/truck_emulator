from aiohttp import web

from app.start import create_app


if __name__ == '__main__':
    app = create_app()
    config = app['config']['http']
    web.run_app(
        app=app,
        host=config['host'],
        port=config.getint('port', 8095)
    )
