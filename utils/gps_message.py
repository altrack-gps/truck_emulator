from random import randint
from datetime import datetime

from serializers import GPSMessage

# remove and stop, if sth done raise an exception if we gonna remove before cancelling task
from utils._encoder import ALTProtoEncoder


def generate(imei, coordinate):
    return GPSMessage(
        imei=imei,
        lat=coordinate[0],
        lon=coordinate[1],
        speed=randint(0, 80),
        course=randint(0, 360),
        state_at=datetime.utcnow()
    )


def encode(gps_message: GPSMessage):
    return ALTProtoEncoder.encode_message(gps_message)
