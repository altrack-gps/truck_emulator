import asyncio
import logging
import functools

from utils import gps_message
from utils.route import build_route
from settings import GPS_SERVICE_CONFIG, SENDING_INTERVAL
from app.exceptions import EmulationExists, NonCancelledTask


class EmulationManager:
    def __init__(self):
        self._tasks = {}

    @property
    def tasks(self):
        return self._tasks

    def get(self, imei: str):
        return self._tasks[imei]

    def add(
            self,
            imei: str,
            task: asyncio.Task
    ):
        if not self.emulation_exists(imei):
            self._tasks[imei] = task
        else:
            raise EmulationExists(f'Emulation for {imei} is already exist')

    def cancel(self, imei):
        if self.emulation_exists(imei):
            self._tasks[imei].cancel()

    def emulation_exists(self, imei):
        return imei in self._tasks

    def remove(self, imei, *args):
        if self.emulation_exists(imei):
            task: asyncio.Task = self._tasks[imei]

            if not task.cancelled() and not task.done():
                raise NonCancelledTask('Task emulation must be cancelled before removing it')

            self._tasks.pop(imei)
            logging.info(f'emulation for {imei} has been finished')


class TruckEmulation:
    @classmethod
    async def create(
            cls,
            imei: str,
            points: list,
            on_remove: callable
    ) -> asyncio.Task:

        coordinates = await build_route(points)
        task: asyncio.Task = asyncio.create_task(
            _run(imei, coordinates)
        )
        task.add_done_callback(
            functools.partial(on_remove, imei)
        )
        return task


async def _run(imei, coordinates):
    _, writer = await asyncio.open_connection(**GPS_SERVICE_CONFIG)
    try:
        for c in coordinates:
            message = gps_message.generate(imei, c)
            encoded_message = gps_message.encode(message)

            writer.write(encoded_message)
            logging.info(f'--- sending {message} ---')

            await asyncio.sleep(SENDING_INTERVAL)
    finally:
        writer.close()
        await writer.wait_closed()


def is_truck_emulating(
        emulation_manager: EmulationManager,
        imei: str
):
    return emulation_manager.emulation_exists(imei)
