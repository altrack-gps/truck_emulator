import aiohttp

from yarl import URL

from settings import ROUTE_BUILDER_API_URL


async def build_route(points):
    url = URL(ROUTE_BUILDER_API_URL) / stringify_points(points)
    params = {
        'geometries': 'geojson',
        'continue_straight': 'false',
        'overview': 'full'
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url=url.with_query(**params)) as response:
            routes = await response.json()
            return change_format(
                routes['routes'][0]['geometry']['coordinates']
            )


def change_format(coordinates: list) -> list:
    return [[c[1], c[0]] for c in coordinates]


def stringify_points(points):
    return ';'.join([f"{p['lng']},{p['lat']}" for p in points])
