from configparser import ConfigParser

from settings import CONFIG_FILE


def read_config(file_name=CONFIG_FILE):
    config = ConfigParser()
    config.read(file_name)

    return config
