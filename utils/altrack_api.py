from yarl import URL
from aiohttp import ClientSession


ALTRACK_URL = URL('http://localhost:8000/api/')


async def fetch_truck_with_route(
        session: ClientSession,
        truck_id: int
) -> dict:
    truck = await fetch_truck(session, truck_id)
    route = await fetch_route(session, truck['routeID'])

    return {
        **truck,
        'route': route
    }


async def fetch_truck(session: ClientSession, truck_id: int):
    path = f'trucks/{truck_id}'
    return await _make_request(session, path)


async def _make_request(
        session: ClientSession,
        path: str
):
    async with session.get(ALTRACK_URL / path) as response:
        return await response.json()


async def fetch_route(session: ClientSession, route_id: int):
    path = f'routes/{route_id}'
    return await _make_request(session, path)


async def fetch_all_trucks(session: ClientSession):
    path = 'trucks/'
    return await _make_request(session, path)
