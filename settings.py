from os import path


CONFIG_FILE = path.join(path.dirname(__file__), 'config.ini')
LOG_FILE = path.join(path.dirname(__file__), 'truck_emulator.log')

SENDING_INTERVAL = 1

ROUTE_BUILDER_API_URL = 'http://localhost:5000/route/v1/driving/'

GPS_SERVICE_CONFIG = {
    'host': 'localhost',
    'port': 8090
}
