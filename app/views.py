import logging
import traceback

from aiohttp import web
from aiohttp_jinja2 import template

from app.exceptions import EmulationExists
from utils.altrack_api import fetch_truck_with_route, fetch_all_trucks
from utils.emulation import TruckEmulation, is_truck_emulating


routes = web.RouteTableDef()


@routes.get('/trucks/')
@template('trucks.html')
async def truck_list_view(request: web.Request):
    session = request.app['session']
    trucks = await fetch_all_trucks(session)

    return {
        'trucks': trucks
    }


@routes.get('/trucks/{id}')
@template('truck_detail.html')
async def truck_detail_view(request: web.Request):
    emulation_manager = request.app['emulation_manager']
    truck_id = request.match_info['id']
    session = request.app['session']
    truck = await fetch_truck_with_route(session, truck_id)

    truck['is_emulating'] = is_truck_emulating(
        emulation_manager=emulation_manager,
        imei=truck['imei']
    )

    return truck


@routes.put('/start-emulation/{imei}', name='start_emulation')
async def start_emulation_view(request: web.Request):
    imei = request.match_info['imei']
    post = await request.json()
    points = post['points']

    emulation_manager = request.app['emulation_manager']
    try:
        task = await TruckEmulation.create(
            imei=imei,
            points=points,
            on_remove=emulation_manager.remove
        )

        emulation_manager.add(imei, task)
        logging.info(f'spawned new task for {imei}')
    except EmulationExists as e:
        return web.HTTPOk(text=str(e))
    except Exception as e:
        logging.error(e, exc_info=traceback.print_exc())

    return web.HTTPCreated()


@routes.put('/stop-emulation/{imei}', name='stop_emulation')
async def stop_emulation_view(request: web.Request):
    imei = request.match_info['imei']
    request.app['emulation_manager'].cancel(imei)

    logging.info(f'cancelled task for {imei}')
    return web.HTTPOk()
