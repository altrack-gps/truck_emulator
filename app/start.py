import jinja2
import logging
import aiohttp
import aiohttp_jinja2

from os import path
from aiohttp import web
from logging.handlers import RotatingFileHandler

from app.views import routes
from settings import LOG_FILE
from utils.config import read_config
from utils.emulation import EmulationManager


def create_app():
    init_logging()
    app = web.Application()

    app['emulation_manager'] = EmulationManager()
    app.on_shutdown.append(stop_emulations)

    app['config'] = read_config()
    app.router.add_routes(routes)

    app.cleanup_ctx.append(init_session)

    aiohttp_jinja2.setup(
        app=app,
        loader=jinja2.FileSystemLoader(path.join(path.dirname(__file__), 'templates'))
    )

    return app


async def stop_emulations(app):
    for task in app['emulation_manager'].tasks.values():
        task.cancel()


async def init_session(app: web.Application):
    app['session'] = session = aiohttp.ClientSession()
    yield
    await session.close()


def init_logging(file_name: str = LOG_FILE):
    log_file = RotatingFileHandler(
        filename=file_name,
        maxBytes=1024 * 1024,
        backupCount=3,
        encoding='utf-8'
    )

    log_file.setLevel(logging.INFO)

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s|truck_emulator|%(filename)20s[%(lineno)3s] %(levelname)8s : %(message)s',
        handlers=[log_file, console]
    )

    logging.basicConfig(level=logging.DEBUG)

